require '../src/element_pricing'
require '../src/checkout'

describe"Checkout" do

  let(:pricing_a) do
    ElementPricing.new(basic_price:  1.5, coupled_amount: 12, coupled_price: 10)
  end

  let(:pricing_b) do
    ElementPricing.new(basic_price:  2, coupled_amount: 5, coupled_price: 7)
  end

  let(:pricing_c) do
    ElementPricing.new(basic_price:  0.8, coupled_amount: 10, coupled_price: 5)
  end

  let(:custom_rules) do
    {
        a: pricing_a,
        b: pricing_b,
        c: pricing_c
    }
  end

  let(:product_list) do
    %w(a a a b a a a a a b a b a a a a a)
  end

  subject do
    Checkout.new custom_rules
  end

  describe 'scan a product' do
     it 'scan a and save the amount of a in the product list' do
      subject.scan('a')
      expect(subject.product_list).to be_eql({'a'=>1})
    end
  end

  describe 'calc the proper amount for the scanned products' do
    before do
      product_list.each do |product|
        subject.scan(product)
      end
    end
      it "should return 19.0" do
        expect(subject.total).to be_eql 19.0
      end
    end
end
