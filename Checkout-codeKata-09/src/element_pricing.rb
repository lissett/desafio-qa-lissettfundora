  # This class has the responsibility to calc a single product type price according with the specified amounts
  class ElementPricing

    # basic price is a single product price coupled amount is the number of items needed for a discount
    # coupled price is the price for coupled items
    def initialize(args)
      @basic_price = args[:basic_price]
      @coupled_amount = args[:coupled_amount] || 1
      @coupled_price = args[:coupled_price] || 0
    end

    # returns the total price for a product amount, applying discounts every time the coupled amount is reached
    def price_for(amount)
      div_result =  amount.divmod(@coupled_amount)
      return @basic_price * amount if div_result[0] < 1
      div_result[0] * @coupled_price + div_result[1] * @basic_price
    end
end
