# This class is responsible for the scan of products and to retrieve the total amount information
class Checkout

  attr_reader :product_list

  # pricing_rules: The hash of pricing rules applied to every product
  ## product_list: The list of scanned products
  def initialize(rules)
    @pricing_rules = rules
    @product_list = {}
  end

  # Scan a single product by default
  # Scan the specified amount of a product if the second attribute is given
  def scan(element, amount = 1)
    return product_list.merge!({"#{element}" => amount}) unless product_list.has_key? element
   product_list[element]+= amount
  end

  # Returns the total price for the scanned products
  def total
    current_total = 0
    product_list.each do |element, amount|
      current_total += @pricing_rules[element.to_sym].price_for(amount)
    end
    current_total
  end
end
