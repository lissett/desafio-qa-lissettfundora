Feature: Mostar Informacion de Contacto

Background:
  Given Existe al menos un contacto

Scenario:  Ver informacion de contacto a partir de la vista de conversacion
  Given estoy en una conversacion con un contacto
  When  selecciono nombre de contacto
  Then me muestra vista de informacion del contacto

Scenario: Ver informacion de contacto a partir de opciones de contacto
  Given selecciono avatar de contacto
  When  escojo la opcion de informacion
  Then me muestra vista de informacion del contacto
